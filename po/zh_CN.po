# Chinese (China) translation for obfuscate.
# Copyright (C) 2020 obfuscate's COPYRIGHT HOLDER
# This file is distributed under the same license as the obfuscate package.
# Damned <damnedlies@163.com>, 2020.
# lumingzh <lumingzh@qq.com>, 2022-2024.
#
msgid ""
msgstr ""
"Project-Id-Version: obfuscate master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/obfuscate/issues\n"
"POT-Creation-Date: 2024-02-18 10:39+0000\n"
"PO-Revision-Date: 2024-02-20 08:37+0800\n"
"Last-Translator: lumingzh <lumingzh@qq.com>\n"
"Language-Team: Chinese - China <i18n-zh@googlegroups.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"X-Generator: Gtranslator 45.3\n"

#: data/com.belmoussaoui.Obfuscate.desktop.in.in:3
#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:7
#: data/resources/ui/window.ui:155 src/application.rs:88
msgid "Obfuscate"
msgstr "Obfuscate"

#: data/com.belmoussaoui.Obfuscate.desktop.in.in:4
#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:8 src/application.rs:89
msgid "Censor private information"
msgstr "模糊处理私人信息"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/com.belmoussaoui.Obfuscate.desktop.in.in:10
msgid "Censor;Private;Image;Obfuscate;"
msgstr "Censor;Private;Image;Obfuscate;审查;隐私;图像;混淆;"

#: data/com.belmoussaoui.Obfuscate.gschema.xml.in:6
msgid "Default window maximized behaviour"
msgstr "默认窗口最大化行为"

#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:10
msgid "Obfuscate lets you redact your private information from any image."
msgstr "Obfuscate 允许您从任何图像中模糊处理您的私人信息。"

#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "主窗口"

#. developer_name tag deprecated with Appstream 1.0
#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:102
msgid "Bilal Elmoussaoui"
msgstr "Bilal Elmoussaoui"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "常规"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Open a New Window"
msgstr "打开新窗口"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Open a File"
msgstr "打开文件"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Load Image from Clipboard"
msgstr "从剪贴板载入图像"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Copy Image to Clipboard"
msgstr "将图像复制到剪贴板"

#: data/resources/ui/shortcuts.ui:38
msgctxt "shortcut window"
msgid "Save As"
msgstr "另存为"

#: data/resources/ui/shortcuts.ui:44
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "显示快捷键"

#: data/resources/ui/shortcuts.ui:50
msgctxt "shortcut window"
msgid "Close the Active Window"
msgstr "关闭活动窗口"

#: data/resources/ui/shortcuts.ui:58
msgctxt "shortcut window"
msgid "Editor"
msgstr "编辑器"

#: data/resources/ui/shortcuts.ui:61
msgctxt "shortcut window"
msgid "Undo"
msgstr "撤销"

#: data/resources/ui/shortcuts.ui:67
msgctxt "shortcut window"
msgid "Redo"
msgstr "重做"

#: data/resources/ui/shortcuts.ui:73
msgctxt "shortcut window"
msgid "Zoom In"
msgstr "放大"

#: data/resources/ui/shortcuts.ui:79
msgctxt "shortcut window"
msgid "Zoom Out"
msgstr "缩小"

#: data/resources/ui/window.ui:9
msgid "_Copy"
msgstr "复制(_C)"

#: data/resources/ui/window.ui:13
msgid "_Save"
msgstr "保存(_S)"

#: data/resources/ui/window.ui:19 data/resources/ui/window.ui:41
msgid "_Open File"
msgstr "打开文件(_O)"

#: data/resources/ui/window.ui:23 data/resources/ui/window.ui:45
msgid "_New Window"
msgstr "新建窗口(_N)"

#: data/resources/ui/window.ui:29 data/resources/ui/window.ui:51
msgid "_Keyboard Shortcuts"
msgstr "键盘快捷键(_K)"

#: data/resources/ui/window.ui:33 data/resources/ui/window.ui:55
msgid "_About Obfuscate"
msgstr "关于 Obfuscate(_A)"

#: data/resources/ui/window.ui:67
msgid "Zoom out"
msgstr "缩小"

#: data/resources/ui/window.ui:74
msgid "Reset Zoom"
msgstr "重置缩放"

#: data/resources/ui/window.ui:79
msgid "100%"
msgstr "100%"

#: data/resources/ui/window.ui:87
msgid "Zoom in"
msgstr "放大"

#: data/resources/ui/window.ui:114
msgid "Select a File"
msgstr "选择一个文件"

#: data/resources/ui/window.ui:119 src/widgets/window.rs:426
msgid "Open"
msgstr "打开"

#: data/resources/ui/window.ui:126 data/resources/ui/window.ui:201
msgid "Main Menu"
msgstr "主菜单"

#: data/resources/ui/window.ui:137
msgid "Drop an Image Here"
msgstr "将图像拖动到此处"

#: data/resources/ui/window.ui:163 data/resources/ui/window.ui:239
msgid "Undo"
msgstr "撤销"

#: data/resources/ui/window.ui:170 data/resources/ui/window.ui:246
msgid "Redo"
msgstr "重做"

#: data/resources/ui/window.ui:184 data/resources/ui/window.ui:260
msgid "Fill"
msgstr "填充"

#: data/resources/ui/window.ui:192 data/resources/ui/window.ui:268
msgid "Blur"
msgstr "模糊"

#: src/application.rs:96
msgid "translator-credits"
msgstr ""
"Boyuan Yang <073plan@gmail.com>, 2020\n"
"lumingzh <lumingzh@qq.com>, 2024"

#: src/widgets/window.rs:228
msgid "Save Changes?"
msgstr "保存更改吗？"

#: src/widgets/window.rs:229
msgid ""
"Open image contain unsaved changes. Changes which are not saved will be "
"permanently lost"
msgstr "打开的图像包含未保存更改。未保存的更改将永久丢失"

#: src/widgets/window.rs:232
msgid "Cancel"
msgstr "取消"

#: src/widgets/window.rs:233
msgid "Discard"
msgstr "丢弃"

#: src/widgets/window.rs:235 src/widgets/window.rs:387
msgid "Save"
msgstr "保存"

#: src/widgets/window.rs:318
msgid ""
"The blur tool is not secure. Please do not use it to share sensitive "
"information in a public forum"
msgstr "模糊工具不安全。请不要使用它来在公共论坛中分享敏感信息"

#: src/widgets/window.rs:386
msgid "Save File"
msgstr "保存文件"

#: src/widgets/window.rs:405
msgid "All Images"
msgstr "全部图像"

#: src/widgets/window.rs:426
msgid "Open File"
msgstr "打开文件"

#~ msgid "Default window x position"
#~ msgstr "默认窗口 X 位置"

#~ msgid "Default window y position"
#~ msgstr "默认窗口 Y 位置"

#~ msgid "Censor private information."
#~ msgstr "模糊处理私人信息。"
