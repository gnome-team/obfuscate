# Dutch translation for obfuscate.
# Copyright (C) 2020 obfuscate's COPYRIGHT HOLDER
# This file is distributed under the same license as the obfuscate package.
# Nathan Follens <nfollens@gnome.org>, 2021-2023.
#
msgid ""
msgstr ""
"Project-Id-Version: obfuscate master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/obfuscate/issues\n"
"POT-Creation-Date: 2023-03-24 13:18+0000\n"
"PO-Revision-Date: 2023-08-27 18:07+0200\n"
"Last-Translator: Nathan Follens <nfollens@gnome.org>\n"
"Language-Team: GNOME-NL https://matrix.to/#/#nl:gnome.org\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.3.2\n"

#: data/com.belmoussaoui.Obfuscate.desktop.in.in:3
#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:7
#: data/resources/ui/window.ui:157 src/application.rs:88
msgid "Obfuscate"
msgstr "Obfuscate"

#: data/com.belmoussaoui.Obfuscate.desktop.in.in:4
#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:8 src/application.rs:89
msgid "Censor private information"
msgstr "Verberg persoonlijke informatie"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/com.belmoussaoui.Obfuscate.desktop.in.in:10
msgid "Censor;Private;Image;Obfuscate;"
msgstr ""
"Censor;Private;Image;Obfuscate;Persoonlijk;Privé;Informatie;Verhullen;"
"Verduisteren;Censureren;"

#: data/com.belmoussaoui.Obfuscate.gschema.xml.in:6
msgid "Default window maximized behaviour"
msgstr "Standaardgedrag van gemaximaliseerd venster"

#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:10
msgid "Obfuscate lets you redact your private information from any image."
msgstr ""
"Obfuscate laat u uw persoonlijke informatie van afbeeldingen verwijderen."

#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "Hoofdvenster"

#: data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in:94
msgid "Bilal Elmoussaoui"
msgstr "Bilal Elmoussaoui"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Algemeen"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Open a New Window"
msgstr "Een nieuw venster openen"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Open a File"
msgstr "Een bestand openen"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Load Image from Clipboard"
msgstr "Afbeelding laden van klembord"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Copy Image to Clipboard"
msgstr "Afbeelding kopiëren naar klembord"

#: data/resources/ui/shortcuts.ui:38
msgctxt "shortcut window"
msgid "Save As"
msgstr "Opslaan als"

#: data/resources/ui/shortcuts.ui:44
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Sneltoetsen tonen"

#: data/resources/ui/shortcuts.ui:50
msgctxt "shortcut window"
msgid "Close the Active Window"
msgstr "Het actieve venster sluiten"

#: data/resources/ui/shortcuts.ui:58
msgctxt "shortcut window"
msgid "Editor"
msgstr "Editor"

#: data/resources/ui/shortcuts.ui:61
msgctxt "shortcut window"
msgid "Undo"
msgstr "Ongedaan maken"

#: data/resources/ui/shortcuts.ui:67
msgctxt "shortcut window"
msgid "Redo"
msgstr "Opnieuw uitvoeren"

#: data/resources/ui/shortcuts.ui:73
msgctxt "shortcut window"
msgid "Zoom In"
msgstr "Inzoomen"

#: data/resources/ui/shortcuts.ui:79
msgctxt "shortcut window"
msgid "Zoom Out"
msgstr "Uitzoomen"

#: data/resources/ui/window.ui:9
msgid "_Copy"
msgstr "_Kopiëren"

#: data/resources/ui/window.ui:13
msgid "_Save"
msgstr "Op_slaan"

#: data/resources/ui/window.ui:19 data/resources/ui/window.ui:41
msgid "_Open File"
msgstr "Bestand _openen"

#: data/resources/ui/window.ui:23 data/resources/ui/window.ui:45
msgid "_New Window"
msgstr "_Nieuw venster"

#: data/resources/ui/window.ui:29 data/resources/ui/window.ui:51
msgid "_Keyboard Shortcuts"
msgstr "Snel_toetsen"

#: data/resources/ui/window.ui:33 data/resources/ui/window.ui:55
msgid "_About Obfuscate"
msgstr "_Over Obfuscate"

#: data/resources/ui/window.ui:67
msgid "Zoom out"
msgstr "Uitzoomen"

#: data/resources/ui/window.ui:74
msgid "Reset Zoom"
msgstr "Zoomniveau herstellen"

#: data/resources/ui/window.ui:79
msgid "100%"
msgstr "100%"

#: data/resources/ui/window.ui:87
msgid "Zoom in"
msgstr "Inzoomen"

#: data/resources/ui/window.ui:114
msgid "Select a File"
msgstr "Selecteer een bestand"

#: data/resources/ui/window.ui:119 src/widgets/window.rs:351
msgid "Open"
msgstr "Openen"

#: data/resources/ui/window.ui:126 data/resources/ui/window.ui:198
msgid "Main Menu"
msgstr "Hoofdmenu"

#: data/resources/ui/window.ui:137
msgid "Drop an Image Here"
msgstr "Versleep een afbeelding hiernaartoe"

#: data/resources/ui/window.ui:162
msgid "Undo"
msgstr "Ongedaan maken"

#: data/resources/ui/window.ui:169
msgid "Redo"
msgstr "Opnieuw uitvoeren"

#: data/resources/ui/window.ui:183
msgid "Fill"
msgstr "Vullen"

#: data/resources/ui/window.ui:191
msgid "Blur"
msgstr "Vervagen"

#: src/application.rs:96
msgid "translator-credits"
msgstr ""
"Nathan Follens <nfollens@gnome.org>\n"
"Meer info over GNOME-NL https://nl.gnome.org"

#: src/widgets/window.rs:247
msgid ""
"The blur tool is not secure. Please do not use it to share sensitive "
"information in a public forum"
msgstr ""
"Het vervagingshulpmiddel is niet veilig. Gebruik het niet om gevoelige "
"informatie op een openbaar forum te delen"

#: src/widgets/window.rs:303
msgid "Save File"
msgstr "Bestand opslaan"

#: src/widgets/window.rs:304
msgid "Save"
msgstr "Opslaan"

#: src/widgets/window.rs:329
msgid "All Images"
msgstr "Alle afbeeldingen"

#: src/widgets/window.rs:352
msgid "Open File"
msgstr "Bestand openen"

#~ msgid "Cancel"
#~ msgstr "Annuleren"

#~ msgid "Default window x position"
#~ msgstr "Standaardpositie van venster op x-as"

#~ msgid "Default window y position"
#~ msgstr "Standaardpositie van venster op y-as"

#~ msgid "Censor private information."
#~ msgstr "Verberg persoonlijke informatie."

#~ msgid "@name-prefix@Obfuscate"
#~ msgstr "@name-prefix@Obfuscate"
